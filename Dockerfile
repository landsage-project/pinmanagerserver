FROM node:16

MAINTAINER Wichayapat Thongrattana

LABEL "version"="1.0.0"
# RUN rm -rf /usr/pinmanager
RUN mkdir -p /usr/pinmanager
WORKDIR /usr/pinmanager

COPY package.json /usr/pinmanager
RUN npm install

COPY ca-certificate.crt /usr/pinmanager

COPY ["index.js","api.html","textureManager.js","mapPin.js","config.json","/usr/pinmanager"] 
RUN mkdir -p /usr/pinmanager/data
RUN mkdir -p /usr/pinmanager/data/beacon
RUN mkdir -p /usr/pinmanager/data/json
RUN mkdir -p /usr/pinmanager/data/csv
RUN mkdir -p /usr/pinmanager/data/wheel
RUN mkdir -p /usr/pinmanager/data/textures
COPY data/textures /usr/pinmanager/data/textures
COPY data/wheel/WheelList.json /usr/pinmanager/data/wheel


EXPOSE 5000

CMD ["npm","start"]